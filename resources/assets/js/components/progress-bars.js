// Progress Bars
(function () {
  'use strict';

  var progressBarWrapper = document.getElementsByClassName('progress-bar');

  Array.prototype.forEach.call(progressBarWrapper, function(el){
    var thisValue = el.getAttribute('data-value');
    el.querySelectorAll('.progress-bar-value')[0].style.width = thisValue + '%';

    console.log(thisValue);
  });
})();
