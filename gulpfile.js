// Requires
// -----------------------------------------------------------------------------
var path = require('path');
var merge = require('merge-stream');

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var paths = require('./paths.json');
var fileinclude = require('gulp-file-include');

function getTask(task) {
  return require('./gulp-tasks/' + task)(
    gulp,
    plugins,
    browserSync,
    reload,
    paths,
    path,
    fileinclude
  );
}

// Tasks
// -----------------------------------------------------------------------------
// Default
gulp.task('assets', [
  'html',
  'sprite',
  'sass',
  'jshint',
  'concat'
]);

// Copies
gulp.task('copies', [
  'copy-images'
]);

// Watches
gulp.task('watch', [
  'watch-images',
  'watch-sprite-images',
  'watch-scss',
  'watch-jshint',
  'watch-js',
  'watch-fonts'
]);

// Build
gulp.task('build', ['assets', 'copies']);

// Default
gulp.task('default', ['build', 'browsersync', 'watch']);

// Minify
gulp.task('min', ['uglify', 'csso', 'imagemin']);

// Single Tasks
// -----------------------------------------------------------------------------
// Stylesheet
gulp.task('sass', getTask('sass'));
gulp.task('csso', getTask('min/csso'));
gulp.task('watch-scss', getTask('watch/watch-scss'));

// Javascript
gulp.task('jshint', getTask('jshint'));
gulp.task('concat', getTask('concat'));
gulp.task('uglify', getTask('min/uglify'));
gulp.task('watch-js', getTask('watch/watch-js'));
gulp.task('watch-jshint', getTask('watch/watch-jshint'));

// Images
gulp.task('sprite', getTask('sprite'));
gulp.task('imagemin', getTask('min/imagemin'));
gulp.task('copy-images', getTask('copy/copy-images'));
gulp.task('watch-images', getTask('watch/watch-images'));
gulp.task('watch-sprite-images', getTask('watch/watch-sprite-images'));

// Others
gulp.task('bump', getTask('bump'));
gulp.task('html', getTask('html'));
gulp.task('browsersync', getTask('browsersync'));
gulp.task('copy-fonts', getTask('copy/copy-fonts'));
gulp.task('watch-fonts', getTask('watch/watch-fonts'));
