module.exports = function(gulp, plugins, browserSync, reload, paths, path, fileinclude) {
  return function () {
    return gulp.src(path.join(paths.src_views, '*.html'))
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(plugins.htmltidy({
      indent: true,
      hideComments: true
    }))
    .pipe(gulp.dest(paths.dest_assets));
  };
};
