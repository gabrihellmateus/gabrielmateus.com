module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return gulp.src([
      // paths.src_vendors + '<file>',
      path.join(paths.src_assets, '/js/**/*.js')
    ])
    .pipe(plugins.debug({ title: 'Build Js files' }))
    .pipe(plugins.concat('app.js'))
    .pipe(gulp.dest(paths.dest_assets + '/js/'));
  };
};
