module.exports = function(gulp, plugins, browserSync, reload, paths) {
  return function () {
    browserSync({
      port: 8010,
      open: false,
      files: [
        paths.dest_assets    +    'js/*.js',
        paths.dest_assets    +    'css/*.css',
        paths.dest_assets    +    'images/*.{png.jpg,gif}',
        paths.dest_assets    +    'fonts/*',
        paths.dest_code      +    '**/*.php'
      ],
      server: "./app"
    });
  };
};
