module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return gulp.src(path.join(paths.dest_assets, '/css/*.css'))
    .pipe(plugins.debug({ title: 'Minifying CSS files' }))
    .pipe(plugins.csso())
    .pipe(gulp.dest(paths.dest_assets + '/css/'));
  };
};
