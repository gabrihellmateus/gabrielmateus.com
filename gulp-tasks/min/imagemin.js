module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  var imageminJpegtran = require('imagemin-jpegtran');
  var imageminPngquant = require('imagemin-pngquant');

  return function () {
    return gulp.src(path.join(paths.src_assets, '/images/**/*.{png,jpg,gif}'))
    .pipe(plugins.size())
    .pipe(plugins.debug({ title: 'Minifying Images files' }))
    .pipe(plugins.imagemin({
      progressive: true,
      use: [imageminPngquant(), imageminJpegtran()]
    }))
    .pipe(plugins.size())
    .pipe(gulp.dest(paths.dest_assets + '/images/'));
  };
};
