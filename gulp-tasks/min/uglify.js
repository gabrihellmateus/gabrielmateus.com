module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return gulp.src(path.join(paths.dest_assets, '/js/*.js'))
    .pipe(plugins.debug({ title: 'Minifying Js files' }))
    .pipe(plugins.uglify())
    .pipe(gulp.dest(paths.dest_assets + '/js/'));
  };
};
