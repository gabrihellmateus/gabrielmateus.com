module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return gulp.src(path.join(paths.src_assets, '/js/**/*.js'))
    .pipe(plugins.debug({ title: 'Hint Js files' }))
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter('jshint-stylish'))
    .pipe(plugins.jshint.reporter('fail'))
    .on('error', plugins.notify.onError({ sound: false }));
  };
};
