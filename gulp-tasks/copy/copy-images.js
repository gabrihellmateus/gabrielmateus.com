module.exports = function(gulp, plugins, browserSync, reload, paths) {
  return function () {
    gulp.src(paths.src_assets + '/images/*.{png,jpg,gif}')
    .pipe(plugins.plumber())
    .pipe(plugins.debug({ title: 'Coping Image files' }))
		.pipe(gulp.dest(paths.dest_assets + '/images/'));
  };
};
