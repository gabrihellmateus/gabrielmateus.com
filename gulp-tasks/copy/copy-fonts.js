module.exports = function(gulp, plugins, browserSync, reload, paths) {
  return function () {
    gulp.src(paths.src_assets + '/fonts/**')
    .pipe(plugins.plumber())
    .pipe(plugins.debug({ title: 'Coping Fonts files' }))
		.pipe(gulp.dest(paths.dest_assets + '/fonts/'));
  };
};
