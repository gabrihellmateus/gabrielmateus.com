module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    var spriteData = gulp.src(path.join(paths.src_assets, '/images/sprite/*.png'))
    .pipe(plugins.debug({ title: 'Create sprite image' }))
    .pipe(plugins.spritesmith({
      imgName: 'sprite.png',
      cssName: '_sprite.scss',
      imgPath: '../images/sprite.png'
    }));
    spriteData.img.pipe(gulp.dest(paths.src_assets + '/images/'));
    spriteData.css.pipe(gulp.dest(paths.src_assets + '/scss/helpers/'));

    return spriteData;
  };
};
