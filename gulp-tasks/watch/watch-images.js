module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return plugins.watch(path.join(paths.src_assets, '/images/**/*.{png,jpg,gif}'))
    .pipe(plugins.plumber())
    .pipe(plugins.debug({ title: 'Watch Image files' }))
    .pipe(plugins.imagemin({
      progressive: true
    }))
    .pipe(gulp.dest(paths.dest_assets + '/images/'))
    .pipe(reload({ stream: true }));
  };
};
