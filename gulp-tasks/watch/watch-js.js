module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return plugins.watch(path.join(paths.src_assets, '/js/**/*.js'), function() {
      gulp.src([
        // paths.src_vendors + '<file>',
        path.join(paths.src_assets, '/js/**/*.js')
      ])
      .pipe(plugins.plumber())
      .pipe(plugins.debug({ title: 'Watch Js files' }))
      .pipe(plugins.concat('app.js'))
      .pipe(gulp.dest(paths.dest_assets + '/js/'))
      .pipe(reload({ stream: true }));
    });
  };
};
