module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return plugins.watch(path.join(paths.src_assets, '/js/**/*.js'), function() {
      gulp.src(path.join(paths.src_assets, '/js/**/*.js'))
      .pipe(plugins.plumber())
      .pipe(plugins.debug({ title: 'Hint Js files' }))
      .pipe(plugins.jshint())
      .pipe(plugins.jshint.reporter('jshint-stylish'))
      .pipe(plugins.jshint.reporter('fail'))
      .on('error', plugins.notify.onError({ sound: false }));
    });
  };
};
