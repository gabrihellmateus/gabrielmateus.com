module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return plugins.watch(path.join(paths.src_assets, '/fonts/**'))
    .pipe(plugins.plumber())
    .pipe(plugins.debug({ title: 'Watch Fonts files' }))
    .pipe(gulp.dest(paths.dest_assets + '/fonts/'))
    .pipe(reload({ stream: true }));
  };
};
