module.exports = function(gulp, plugins, browserSync, reload, paths, path) {
  return function () {
    return plugins.watch(path.join(paths.src_assets, '/scss/**/*.scss'), function() {
    gulp.src(path.join(paths.src_assets, '/scss/*.scss'))
      .pipe(plugins.plumber({
        errorHandler: plugins.notify.onError({
          sound: false,
          message: "SHIT! - <%= error.message %>"
        })
      }))
      .pipe(plugins.debug({ title: 'Watch SCSS files' }))
      .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass({
            sourcemap: true
          })
        )
        .pipe(plugins.autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
      .pipe(plugins.combineMq({ beautify: true }))
      .pipe(plugins.sourcemaps.write('.'))
      .pipe(gulp.dest(paths.dest_assets + '/css/'))
      .pipe(reload({ stream: true }));
    });
  };
};
